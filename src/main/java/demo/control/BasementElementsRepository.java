package demo.control;

import demo.data.BasementElement;
import org.springframework.data.repository.CrudRepository;

public interface BasementElementsRepository extends CrudRepository<BasementElement, Integer> {
}
