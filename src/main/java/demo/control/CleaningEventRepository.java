package demo.control;

import demo.data.CleaningEvent;
import org.springframework.data.repository.CrudRepository;

public interface CleaningEventRepository extends CrudRepository<CleaningEvent, Integer> {

}
