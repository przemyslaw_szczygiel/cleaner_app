package demo.control;

import demo.data.BasementCleaningEvent;
import org.springframework.data.repository.CrudRepository;

public interface BasementCleaningEventRepository extends CrudRepository<BasementCleaningEvent, Integer> {
}
