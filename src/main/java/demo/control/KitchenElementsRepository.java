package demo.control;

import demo.data.KitchenElement;
import org.springframework.data.repository.CrudRepository;

public interface KitchenElementsRepository extends CrudRepository<KitchenElement, Integer>{
}

