package demo.control;

import demo.data.BathroomElement;
import org.springframework.data.repository.CrudRepository;

public interface BathroomElementsRepository extends CrudRepository<BathroomElement, Integer>{
}