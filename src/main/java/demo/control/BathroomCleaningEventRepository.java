package demo.control;

import demo.data.BathroomCleaningEvent;
import org.springframework.data.repository.CrudRepository;

public interface BathroomCleaningEventRepository extends CrudRepository<BathroomCleaningEvent, Integer> {
}
