package demo.data;

import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Observable;
import java.util.Observer;

@Table(name = "USERS")

@Entity
public class User implements Observer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    private String userName;
    private String email;
    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("CleanerApp: adding new note" + arg);
    }
}
