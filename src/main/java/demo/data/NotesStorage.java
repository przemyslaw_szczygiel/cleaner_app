package demo.data;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

@Component
public class NotesStorage extends Observable {
    private ArrayList<Note> notes = new ArrayList<>();

    public void addNote(Note note) {
        notes.add(note);
        setChanged();
        notifyObservers(note);
    }

}