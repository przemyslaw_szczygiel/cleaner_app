package demo.boundary;


import demo.control.UsersRepository;
        import demo.data.Login;
import demo.data.NotesStorage;
import demo.data.User;
        import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

        import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
        import java.security.NoSuchAlgorithmException;
        import java.util.ArrayList;
        import java.util.Iterator;
        import java.util.List;
        import java.util.UUID;

@CrossOrigin
@RestController
public class UserController
{
    public static List<String> activeTokens = new ArrayList<>();
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private NotesStorage notesStorage;
    @PutMapping(path = "/users", consumes = "application/json")
    public void putUser(@RequestBody User user)
    {
        User userFirst = usersRepository.findByUserName(user.getUserName());
        if (userFirst == null)
        {
            System.out.println("Brawo! Nie ma jeszcze użytkownika o podanej nazwie!");
            try
            {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                md5.update(user.getPassword().getBytes());
                byte[] digest = md5.digest();
                String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
                user.setPassword(myHash);
            }
            catch (NoSuchAlgorithmException e)
            {
                System.out.println("no algoritm");
            }
// save user in database
            usersRepository.save(user);
        }
        else
            {
            System.out.println("Użytkownik o podanej nazwie już istnieje");
        }
    }
    // read list of users
    @RequestMapping(path = "/users")
    public List<User> getAllUsers()
    {
        Iterable<User> users = usersRepository.findAll();
        List<User> userList = new ArrayList<>();
        Iterator<User> iterator = users.iterator();
        while (iterator.hasNext())
        {
            userList.add(iterator.next());
        }
        return userList;
    }
    @PutMapping(path = "/login2", consumes = "application/json")
    public String putUser(@RequestBody Login login)
    {
        User user = usersRepository.findByEmail(login.getEmail());
        if (user == null)
        {
            System.out.println("Nie istnieje taki użytkownik.");
            return "NO_USER";
        }
        else
            {
            String myHash = null;
            try
            {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                md5.update(login.getPassword().getBytes());
                byte[] digest = md5.digest();
                myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
            }
            catch (NoSuchAlgorithmException e)
            {
                System.out.println("no algoritm");
            }
            if (user.getPassword().equals(myHash))
            {
                System.out.println("Użytkownik o podanej nazwie istnieje.");
                String uuid = UUID.randomUUID().toString();
                activeTokens.add(uuid);
                notesStorage.addObserver(user);
                return uuid + "," + user.getUserName();
            }
            else
                {
                System.out.println("Błąd hasła!");
                return "WRONG_PASSWORD!";
            }
        }
    }
    @PutMapping(path = "/logout", consumes = "application/json")
    public String logoutUser(@RequestHeader("token") String myActiveToken)
    {
        activeTokens.remove(myActiveToken);
        System.out.println("Użytkownik został poprawnie wylogowany.");
        return "NO_SESSION";
    }
}
