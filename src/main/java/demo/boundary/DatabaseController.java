package demo.boundary;

import demo.control.*;
import demo.data.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.time.Instant;
import java.util.*;

@CrossOrigin
@RestController
public class DatabaseController
{
    private void tokenAuthorisation(String myActiveToken)
    {
        boolean exists = UserController.activeTokens.contains(myActiveToken);
        if (exists == true)
        {
            System.out.println("Istnieje taki token:)");
        }
        else
            {
            System.out.println("Nie ma takiego tokenu");
            throw new RuntimeException();
        }
    }
    @Autowired
    private BathroomElementsRepository bathroomElementsRepository;
    @Autowired
    private KitchenElementsRepository kitchenElementsRepository;
    @Autowired
    private BasementElementsRepository basementElementsRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private NotesRepository notesRepository;
    @Autowired
    private NotesStorage notesStorage;
    @Autowired
    private CleaningEventRepository cleaningEventRepository;
    @Autowired
    private BasementCleaningEventRepository basementCleaningEventRepository;
    @Autowired
    private BathroomCleaningEventRepository bathroomCleaningEventRepository;
    @PostMapping(path = "/bathroomElements", consumes = "application/json")
    public void postBathroomElements(@RequestBody BathroomElement bathroomElement, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        bathroomElementsRepository.save(bathroomElement);
    }
    @RequestMapping(path = "/bathroomElements")
    public List<BathroomElement> getAllAuthors(@RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        Iterable<BathroomElement> bathroomElements = bathroomElementsRepository.findAll();
        List<BathroomElement> bathroomElementList = new ArrayList<>();
        Iterator<BathroomElement> iterator = bathroomElements.iterator();
        while (iterator.hasNext())
        {
            bathroomElementList.add(iterator.next());
        }
        return bathroomElementList;
    }
    @DeleteMapping(path = "/bathroomElements/{id}")
    public void deleteBathroomElement(@PathVariable int id, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        bathroomElementsRepository.deleteById(id);
    }
    @PutMapping(path = "/bathroomElements")
    public void putBathroomElements(@RequestBody BathroomElement bathroomElement, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        bathroomElementsRepository.save(bathroomElement);
    }
    @PostMapping(path = "/kitchenElements", consumes = "application/json")
    public void postKitchenElement(@RequestBody KitchenElement kitchenElement, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        kitchenElementsRepository.save(kitchenElement);
    }
    @RequestMapping(path = "/kitchenElements")
    public List<KitchenElement> getAllKitchenElements(@RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        Iterable<KitchenElement> kitchenElements = kitchenElementsRepository.findAll();
        List<KitchenElement> kitchenElementsList = new ArrayList<>();
        Iterator<KitchenElement> iterator = kitchenElements.iterator();
        while (iterator.hasNext())
        {
            kitchenElementsList.add(iterator.next());
        }
        return kitchenElementsList;
    }
    @DeleteMapping(path = "/kitchenElements/{id}")
    public void deleteKitchenElement(@PathVariable int id, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        kitchenElementsRepository.deleteById(id);
    }
    @PutMapping(path = "/kitchenElements")
    public void putKitchenElement(@RequestBody KitchenElement kitchenElement, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        kitchenElementsRepository.save(kitchenElement);
    }
    @PostMapping(path = "/notes", consumes = "application/json")
    public void postNotes(@RequestBody Note note, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        notesRepository.save(note);
    }
    @RequestMapping(path = "/notes")
    public List<Note> getAllNotes(@RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        Iterable<Note> notes = notesRepository.findAll();
        List<Note> notesList = new ArrayList<>();
        Iterator<Note> iterator = notes.iterator();
        while (iterator.hasNext())
        {
            notesList.add(iterator.next());
        }
        return notesList;
    }
    @PutMapping(path = "/notes")
    public void putNote(@RequestBody Note note, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        notesStorage.addNote(note);
        notesRepository.save(note);
    }
    @DeleteMapping(path = "/notes/{id}")
    public void deleteNote(@PathVariable int id, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        notesRepository.deleteById(id);
    }

    @PostMapping(path = "/basementElements", consumes = "application/json")
    public void postBasementElements(@RequestBody BasementElement basementElement, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        basementElementsRepository.save(basementElement);
    }

    @RequestMapping(path = "/basementElements")
    public List<BasementElement> getAllBasementElements(@RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        Iterable<BasementElement> basementElements = basementElementsRepository.findAll();
        List<BasementElement> basementElementsList = new ArrayList<>();
        Iterator<BasementElement> iterator = basementElements.iterator();
        while (iterator.hasNext())
        {
            basementElementsList.add(iterator.next());
        }
        return basementElementsList;
    }

    @PutMapping(path = "/basementElements")
    public void putBasementElement(@RequestBody BasementElement basementElement, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        basementElementsRepository.save(basementElement);
    }

    @DeleteMapping(path = "/basementElements/{id}")
    public void deleteBasementElement(@PathVariable int id, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        basementElementsRepository.deleteById(id);
    }

    @PostMapping(path = "/bathroomCleaningEvents", consumes = "application/json")
    public void postBathroomCleaningEvent(@RequestBody BathroomCleaningEvent bathroomCleaningEvent, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        bathroomCleaningEvent.setTimestamp(Instant.now());
        bathroomCleaningEventRepository.save(bathroomCleaningEvent);
    }
    @RequestMapping(path = "/bathroomCleaningEvents")
    public List<BathroomCleaningEvent> getAllBathroomCleaningEvents(@RequestHeader("token") String myActiveToken)
    {
        long start = System.nanoTime();
        tokenAuthorisation(myActiveToken);
        Iterable<BathroomCleaningEvent> bathroomCleaningEvents = bathroomCleaningEventRepository.findAll();
        List<BathroomCleaningEvent> bathroomCleaningEventList = new ArrayList<>();
        Iterator<BathroomCleaningEvent> iterator = bathroomCleaningEvents.iterator();
        while (iterator.hasNext())
        {
            BathroomCleaningEvent next = iterator.next();
            User usr = usersRepository.findByEmail(next.getEmail());
            next.setUserName(usr.getUserName());
            bathroomCleaningEventList.add(next);
            String bathroomIdsInString = next.getIds();
            String[] items = bathroomIdsInString.split(",");
            List<String> bIdsSplit = Arrays.asList(items);
            ArrayList<Integer> bathroomIdsInInt = new ArrayList<>();
            for(int i = 0; i < bIdsSplit.size(); i++)
            {
                bathroomIdsInInt.add(Integer.parseInt(bIdsSplit.get(i)));
            }
            String listOfNames = "";
            for(Integer i : bathroomIdsInInt)
            {
                Optional<BathroomElement> be = bathroomElementsRepository.findById(i);
                if(be.isPresent())
                {
                    listOfNames += be.get().getItem() + ",";
                }
                else
                {
                    listOfNames += "removed" + ",";
                }
            }
            next.setItem(listOfNames);
            long finish = System.nanoTime();
            long timeElapsed = finish - start;
            System.out.println(timeElapsed);
        }
        return bathroomCleaningEventList;
    }
    @PostMapping(path = "/kitchenCleaningEvents", consumes = "application/json")
    public void postCleaningEvent(@RequestBody CleaningEvent cleaningEvent, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        cleaningEvent.setTimestamp(Instant.now());
        cleaningEventRepository.save(cleaningEvent);
    }
    @RequestMapping(path = "/kitchenCleaningEvents")
    public List<CleaningEvent> getAllKitchenCleaningEvents(@RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        Iterable<CleaningEvent> cleaningEvents = cleaningEventRepository.findAll();
        List<CleaningEvent> cleaningEventList = new ArrayList<>();
        Iterator<CleaningEvent> iterator = cleaningEvents.iterator();
        while(iterator.hasNext())
        {
            CleaningEvent next = iterator.next();
            User usr = usersRepository.findByEmail(next.getEmail());
            next.setUserName(usr.getUserName());
            cleaningEventList.add(next);
            String kitchenIdsInString = next.getIds();
            String[] items = kitchenIdsInString.split(",");
            List<String> kIdsSplit = Arrays.asList(items);
            ArrayList<Integer> kitchenIdsInInt = new ArrayList<Integer>();
            for(int i = 0; i < kIdsSplit.size(); i++)
            {
                kitchenIdsInInt.add(Integer.parseInt(kIdsSplit.get(i)));
            }
            String listOfNames = "";
            for(Integer i : kitchenIdsInInt)
            {
                KitchenElement ke = kitchenElementsRepository.findById(i).orElse(null);
                if(ke == null)
                {
                    listOfNames += "removed" + ",";
                }
                else {
                    listOfNames += ke.getItem() + ",";
                }
            }
            next.setItem(listOfNames);
        }
        return cleaningEventList;
    }

    @PostMapping(path = "/basementCleaningEvents", consumes = "application/json")
    public void postBasementCleaningEvent(@RequestBody BasementCleaningEvent basementCleaningEvent, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        basementCleaningEvent.setTimestamp(Instant.now());
        basementCleaningEventRepository.save(basementCleaningEvent);
    }

    @RequestMapping(path = "/basementCleaningEvents")
    public List<BasementCleaningEvent> getAllBasementCleaningEvents(@RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        Iterable<BasementCleaningEvent> basementCleaningEvents = basementCleaningEventRepository.findAll();
        List<BasementCleaningEvent> basementCleaningEventList = new ArrayList<>();
        Iterator<BasementCleaningEvent> iterator = basementCleaningEvents.iterator();
        while(iterator.hasNext())
        {
            BasementCleaningEvent next = iterator.next();
            User usr = usersRepository.findByEmail(next.getEmail());
            next.setUserName(usr.getUserName());
            basementCleaningEventList.add(next);
            String basementIdsInString = next.getIds();
            String[] items = basementIdsInString.split(",");
            List<String> bsIdsSplit = Arrays.asList(items);
            ArrayList<Integer> basementIdsInInt = new ArrayList<Integer>();
            for(int i = 0; i < bsIdsSplit.size(); i++)
            {
                basementIdsInInt.add(Integer.parseInt(bsIdsSplit.get(i)));
            }
            String listOfNames = "";
            for(Integer i : basementIdsInInt)
            {
                BasementElement bse = basementElementsRepository.findById(i).orElse(null);
                if(bse == null)
                {
                    listOfNames += "removed" + ",";
                }
                else {
                    listOfNames += bse.getItem() + ",";
                }
            }
            next.setItem(listOfNames);
        }
        return basementCleaningEventList;
    }
}
