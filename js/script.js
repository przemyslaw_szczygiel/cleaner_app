var kitchenEl;
var bathEl;
var basEl;
var myActiveToken;
var indexOfUpdatingBathItem;
var indexOfUpdatingKitchItem;
var notes;
var login;
var cleaningEvent;
var readFun;
var baseUrl = 'http://localhost:8080/';
var host;
var port;

function addUser() {
    var user = new Object();
    user.userName = document.getElementsByName("user_name")[0].value;
    user.email = document.getElementsByName("user_email")[0].value;
    user.password = document.getElementsByName("user_pass")[0].value;
    var userJson = JSON.stringify(user);
    console.log(userJson);
    const Http = new XMLHttpRequest();
    const url = baseUrl + 'users';
    Http.open("PUT", url);
    Http.setRequestHeader("Content-type", "application/json");
    Http.send(userJson);
    alert('User added!');
}

function logInUser() {
    login = new Object();
    login.email = document.getElementsByName("log_email")[0].value;
    login.password = document.getElementsByName("log_pass")[0].value;
    var loginJson = JSON.stringify(login);
    console.log(loginJson);
    const Http = new XMLHttpRequest();
    const url = baseUrl + 'login2';
    Http.open("PUT", url);
    Http.setRequestHeader("Content-type", "application/json");
    Http.send(loginJson);
    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            console.log(Http.responseText)
            if (Http.responseText === 'NO_USER') {
                alert("There is no user!");
            } else {
                if (Http.responseText === 'WRONG_PASSWORD!') {
                    alert("Password error");
                } else {
                    myActiveToken = Http.responseText.split(",", 1)[0];
                    document.getElementById("login").style.display = "none";
                    document.getElementById("cleaning").style.display = "block";
                    var nameToRead = (Http.responseText).split(",", 2)[1];
                    document.getElementById("emailbleble").innerHTML = nameToRead + ", ";
                }
            }
        }
    }
}

function logMeOut() {
    //	it loges out the user 
    //	it direct it into log in interface
    const Http = new XMLHttpRequest();
    const url = baseUrl + 'logout';
    Http.open("PUT", url);
    Http.setRequestHeader("token", myActiveToken);
    // send token into backend	
    Http.setRequestHeader("Content-type", "application/json");
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            console.log(Http.responseText)
            if (Http.responseText === 'NO_SESSION') {
                alert("Logged out successfully!");
                document.getElementById("login").style.display = "block";
                document.getElementById("cleaning").style.display = "none";
                document.getElementById("emailbleble").innerHTML = "";
            }
        }
    }
}

function addElement(place, kin) {
    var item = new Object();
    item.item = document.getElementsByName(kin)[0].value;
    var itemJson = JSON.stringify(item);
    console.log(itemJson);
    const Http = new XMLHttpRequest();
    const url = baseUrl + place + 'Elements';
    Http.open("PUT", url);
    Http.setRequestHeader("token", myActiveToken);
    Http.setRequestHeader("Content-type", "application/json");
    Http.send(itemJson);
    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            console.log(Http.responseText)
            readList(place);
        }
    }
}

function readList(place) {
    if (place == 'kitchen') {
        document.getElementById("carousel").style.display = "none";
        document.getElementById("kitchen_list").style.display = "block";
        document.getElementById("kitchen_elements").style.display = "block";
        document.getElementById("kitchenLastCleaning").style.display = "none";
        document.getElementById("cleaning_and_notes").style.display = "none";
        document.getElementById("bathroom_list").style.display = "none";
		document.getElementById("basement_list").style.display = "none";
		document.getElementById("basement_elements").style.display = "none";
    } else if (place == 'bathroom') {
        document.getElementById("carousel").style.display = "none";
        document.getElementById("bathroom_list").style.display = "block";
        document.getElementById("bathroom_elements").style.display = "block";
		document.getElementById("basement_list").style.display = "none";
		document.getElementById("basement_elements").style.display = "none";
        document.getElementById("bathroomLastCleaning").style.display = "none";
        document.getElementById("cleaning_and_notes").style.display = "none";
        document.getElementById("kitchen_list").style.display = "none";
    } else if (place == 'basement') {
		document.getElementById("carousel").style.display = "none";
		document.getElementById("basement_list").style.display = "block";
		document.getElementById("basement_elements").style.display = "block";
        document.getElementById("kitchen_list").style.display = "none";
        document.getElementById("kitchen_elements").style.display = "none";
        document.getElementById("basementLastCleaning").style.display = "none";
        document.getElementById("cleaning_and_notes").style.display = "none";
        document.getElementById("bathroom_list").style.display = "none";
	}
    const Http = new XMLHttpRequest();
    const url = baseUrl + place + 'Elements';
    Http.open("GET", url);
    Http.setRequestHeader("token", myActiveToken);
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            console.log(Http.responseText)
            document.getElementById(place + "_elements").innerHTML = "<h3>Check <span><img src=\"images/check.png\" height='30'></span> if it is cleaned!</h3>";
            var obj = JSON.parse(Http.responseText);
            if (place == 'kitchen') {
                kitchenEl = obj;
                for (i = 0; i < obj.length; i++) {
                    kitchenBlock(obj);
                }
            } else if (place == 'bathroom') {
                bathEl = obj;
                for (i = 0; i < obj.length; i++) {
                    bathroomBlock(obj);
                }
            } else if (place == 'basement') {
				basEl = obj;
				for (i = 0; i < obj.length; i++) {
                    basementBlock(obj);
                }
			}
            for (i = 0; i < obj.length; i++) {
                document.getElementById("my_checked_" + place + i).checked = obj[i].checked;
                if (obj[i].checked == true) {
                    document.getElementById(place + i).style.textDecoration = "line-through";
                }
            }
        }
    }
}

function kitchenBlock(obj) {
    document.getElementById("kitchen_elements").innerHTML += "<table><tr><td style=\"width: 50%\" id=\"kitchen" + i + "\">" +
        obj[i].item + "</td><td>" + createChechBoxKitch(i) + "</td><td>" +
        "<button class=\"btn btn-light mb-2\" id=\"edit_kitchen_item\" onclick=\"editKitchenItem(" + i + ")\"><img src=\"images/edit.png\" height='32'></button>" +
        "</td><td>" +
        "<button class=\"btn btn-light mb-2\" id=\"edit_kitchen_item\" onclick=\"deleteItem('kitchenElements'," + obj[i].id + ")\"><img src=\"images/rubbish-bin.png\" height='32'></button>" +
        "</td></tr></table>" +
        "<br>";
}

function bathroomBlock(obj) {
    document.getElementById("bathroom_elements").innerHTML += "<table><tr><td style=\"width: 50%\" id=\"bathroom" + i + "\">" +
        obj[i].item + "</td><td>" + createCheckBoxBath(i) + "</td><td>" +
        "<button class=\"btn btn-light mb-2\" id=\"edit_bathroom_item\" onclick=\"editBathroomItem(" + i + ")\">" +
        "<img src=\"images/edit.png\" height='32' class=\"icon-trash\"></button>" + "</td><td>" +
        "<button class=\"btn btn-light mb-2\" id=\"edit_bathroom_item\" onclick=\"deleteItem('bathroomElements', " + obj[i].id + ")\"><img src=\"images/rubbish-bin.png\" height='32'></button>" +
        "</td></tr></table>" +
        "<br>";
}

function basementBlock(obj) {
    document.getElementById("basement_elements").innerHTML += "<table><tr><td style=\"width: 50%\" id=\"basement" + i + "\">" +
        obj[i].item + "</td><td>" + createCheckBoxBas(i) + "</td><td>" +
        "<button class=\"btn btn-light mb-2\" id=\"edit_basement_item\" onclick=\"editBasementItem(" + i + ")\">" +
        "<img src=\"images/edit.png\" height='32' class=\"icon-trash\"></button>" + "</td><td>" +
        "<button class=\"btn btn-light mb-2\" id=\"edit_basement_item\" onclick=\"deleteItem('basementElements', " + obj[i].id + ")\"><img src=\"images/rubbish-bin.png\" height='32'></button>" +
        "</td></tr></table>" +
        "<br>";
}

function createChechBoxKitch(i) {
    return "<input id=\"my_checked_kitchen" + i + "\" type=\"checkbox\" onclick=\"check('kitchen', " + i + ")\">";
}

function createCheckBoxBath(i) {
    return "<input id=\"my_checked_bathroom" + i + "\" type=\"checkbox\" onclick=\"check('bathroom', " + i + ")\">";
}

function createCheckBoxBas(i) {
    return "<input id=\"my_checked_basement" + i + "\" type=\"checkbox\" onclick=\"check('basement', " + i + ")\">";
}

function backToPlacesList() {
    document.getElementById("kitchen_list").style.display = "none";
    document.getElementById("bathroom_list").style.display = "none";
	document.getElementById("basement_list").style.display = "none";
    document.getElementById("cleaning_and_notes").style.display = "none";
    document.getElementById("carousel").style.display = "block";
}
// function creates fragment in html fo editing element from bathroom list
function editBathroomItem(i) {
    document.getElementById("bathroom" + i).innerHTML = "<div><input type=\"text\" name=\"bathroomItemName\" style=\"width: 70%\"><br>" +
        "<button class=\"btn btn-light mb-2\" onclick=\"updateItem('bathroom'," + i + ")\">Submit!</button></div>";
}
// function creates fragment in html fo editing element from kitchen list
function editKitchenItem(i) {
    document.getElementById("kitchen" + i).innerHTML = "<div><input type=\"text\" name=\"kitchenItemName\" style=\"width: 70%\"><br>" +
        "<button class=\"btn btn-light mb-2\" onclick=\"updateItem('kitchen', " + i + ")\">Submit!</button></div>";
}
// function creates fragment in html fo editing element from basement list
function editBasementItem(i) {
    document.getElementById("basement" + i).innerHTML = "<div><input type=\"text\" name=\"basementItemName\" style=\"width: 70%\"><br>" +
        "<button class=\"btn btn-light mb-2\" onclick=\"updateItem('basement', " + i + ")\">Submit!</button></div>";
}
// function edits element from bathroom list,
// send it to database and refresh website
function updateItem(place, index) {
    var item = new Object();
    var obj;
    if (place == 'bathroom') {
        obj = bathEl;
    } else if (place == 'kitchen') {
        obj = kitchenEl;
    } else if (place == 'basement') {
		obj = basEl;
	}
    item.id = obj[index].id;
    item.item = document.getElementsByName(place + "ItemName")[0].value;
    var itemJson = JSON.stringify(item);
    console.log(itemJson);
    const Http = new XMLHttpRequest();
    const url = baseUrl + place + 'Elements';
    Http.open("PUT", url);
    Http.setRequestHeader("token", myActiveToken);
    Http.setRequestHeader("Content-type", "application/json");
    Http.send(itemJson);
    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            console.log(Http.responseText)
            alert("Item updated!");
            readList(place);
        }
    }
}
// function deletes elements from the list
function deleteItem(link, id) {
    const Http = new XMLHttpRequest();
    const url = baseUrl + link + '/' + id;
    Http.open("DELETE", url);
    Http.setRequestHeader("token", myActiveToken);
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            console.log(Http.responseText)
        }
    }
    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            console.log(Http.responseText)
            alert("Item deleted!");
            if (link == "kitchenElements")
                // if you deleted element from kitchen list
                readList('kitchen');
            else if (link == "bathroomElements")
                // if you deleted element from bathroom list
                readList('bathroom');
			else if (link == "basementElements")
                // if you deleted element from basement list
                readList('basement');
            else {
                // if you deleted notes element
                readNotes();
            }
        }
    }
}

function check(place, i) {
    var obj;
    if (place == 'bathroom') {
        obj = bathEl;
    } else if (place == 'kitchen') {
        obj = kitchenEl;
	} else if (place == 'basement') {
        obj = basEl;
    }
    var checked = document.getElementById('my_checked_' + place + i);
    if (checked.checked == true) {
        document.getElementById(place + i).style.textDecoration = "line-through";
        obj[i].checked = true;
    } else {
        document.getElementById(place + i).style.textDecoration = "none";
        obj[i].checked = false;
    }
    toggleItem(place, i);
}

function toggleItem(place, i) {
    var obj;
    if (place == 'bathroom') {
        obj = bathEl;
    } else if (place == 'kitchen') {
        obj = kitchenEl;
    } else if (place == 'basement') {
        obj = basEl;
    }
    var itemJson = JSON.stringify(obj[i]);
    console.log(itemJson);
    const Http = new XMLHttpRequest();
    const url = baseUrl + place + 'Elements';
    Http.open("PUT", url);
    Http.setRequestHeader("token", myActiveToken);
    Http.setRequestHeader("Content-type", "application/json");
    Http.send(itemJson);
}

function readNotes() {
    document.getElementById("carousel").style.display = "none";
    document.getElementById("cleaning_and_notes").style.display = "block";
    document.getElementById("bathroom_list").style.display = "none";
	document.getElementById("basement_list").style.display = "none";
    document.getElementById("kitchen_list").style.display = "none";
    const Http = new XMLHttpRequest();
    const url = baseUrl + 'notes';
    Http.open("GET", url);
    Http.setRequestHeader("token", myActiveToken);
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            console.log(Http.responseText)
            document.getElementById("notes").innerHTML = "";
            var obj = JSON.parse(Http.responseText);
            notes = obj;
            for (i = 0; i < obj.length; i++) {
                document.getElementById("notes").innerHTML += "<table><tr><td style=\"width: 70%\" id=\"note" + i + "\">" + obj[i].content + "</td>" +
                    "<td><button class=\"btn btn-light mb-2\" onclick=\"deleteItem('notes', " + obj[i].id + ")\"><img src=\"images/rubbish-bin.png\" height='32'></button></td></tr></table>" +
                    "<br>";
            }
        }
    }
}

function addNote(nc) {
    var noteContent = new Object();
    noteContent.content = document.getElementsByName(nc)[0].value;
    var noteContentJson = JSON.stringify(noteContent);
    console.log(noteContentJson);
    const Http = new XMLHttpRequest();
    const url = baseUrl + 'notes';
    Http.open("PUT", url);
    Http.setRequestHeader("token", myActiveToken);
    Http.setRequestHeader("Content-type", "application/json");
    Http.send(noteContentJson);
    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            console.log(Http.responseText)
            alert("You've just added note!");
            readNotes();
        }
    }
}

function finishCleaning(place) {
    var cleaningEvent = new Object();
    cleaningEvent.ids = "";
    var obj;
    if (place == 'kitchen') {
        obj = kitchenEl;
    } else if (place == 'bathroom') {
        obj = bathEl;
    } else if (place == 'basement') {
        obj = basEl;
    }
    for (var i = 0; i < obj.length; i++) {
        if (obj[i].checked == true) {
            cleaningEvent.ids += obj[i].id + ",";
        }
    }
    cleaningEvent.email = login.email;
    var cleaningEventJson = JSON.stringify(cleaningEvent);
    console.log(cleaningEventJson);
    const Http = new XMLHttpRequest();
    const url = baseUrl + place + 'CleaningEvents';
    Http.open("POST", url);
    Http.setRequestHeader("token", myActiveToken);
    Http.setRequestHeader("Content-type", "application/json");
    Http.send(cleaningEventJson);
    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            console.log(Http.responseText)
			alert("You've just added cleaning event!");
        }
    }
}

function showLastCleaning(place) {
    var obj;

    function replaceComma(stringOfItems) {
        var itemName = stringOfItems;
        var itemNameSplit = itemName.replace(/,(?!.*,)/, '.');
        var itemNameSplit = itemNameSplit.replace(/,/g, ", ");
        return itemNameSplit;
    }

    function changeDateFormat(newDate) {
        var date = newDate;
        var newFormat = date.split("T")[0];
        return newFormat;
    }
    if (place == 'kitchen') {
        obj = kitchenEl;
    } else if (place == 'bathroom') {
        obj = bathEl;
    } else if (place == 'basement') {
        obj = basEl;
    }
    document.getElementById(place + "LastCleaning").style.display = "block";
    document.getElementById(place + "_elements").style.display = "none";
    const Http = new XMLHttpRequest();
    const url = baseUrl + place + 'CleaningEvents';
    Http.open("GET", url);
    Http.setRequestHeader("token", myActiveToken);
    Http.setRequestHeader("Content-type", "application/json");
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            console.log(Http.responseText)
            document.getElementById(place + "LastCleaning").innerHTML = "<h3>Last cleaning!</h3>";
            var obj = JSON.parse(Http.responseText);
            cleaningEvent = obj;
            for (i = 0; i < obj.length; i++) {
                document.getElementById(place + "LastCleaning").innerHTML += "<p>" + changeDateFormat(obj[i].timestamp) + " user <b>" + obj[i].userName +
                    "</b> cleaned: <br>" + replaceComma(obj[i].item) + "</p>";
            }
            document.getElementById(place + "LastCleaning").innerHTML += "<a href=\"#\" onclick=\"readList('" + place + "')\" class=\"badge badge-danger\">Back</a>";
        }
    }
}